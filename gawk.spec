%global gawk_api_major %%(x=`tar -xf %{SOURCE0} gawk-%{version}/gawkapi.h --to-stdout 2>/dev/null | \
                          grep -i -e "gawk_api_major.*[[:digit:]]" | \
                          grep -o -e "[[:digit:]]"`; \
			  [ "$x" -lt 3 ] && x=3; echo $x)

%global gawk_api_minor %%(tar -xf %{SOURCE0} gawk-%{version}/gawkapi.h --to-stdout 2>/dev/null | \
                          grep -i -e "gawk_api_minor.*[[:digit:]]" | \
                          grep -o -e "[[:digit:]]" || :)
Name:		gawk
Version:	5.3.1
Release:	1
License:	GPL-3.0-or-later AND GPL-2.0-or-later AND LGPL-2.1-or-later AND BSD-3-Clause
Summary:	The GNU version of the AWK text processing utility
URL:		https://www.gnu.org/software/gawk/
Source0:	https://ftp.gnu.org/gnu/gawk/gawk-%{version}.tar.xz

Patch1:         Disable-pma-test.awk.patch

BuildRequires:	gcc automake grep make gcc
BuildRequires:	bison texinfo >= 7.0.1 texinfo-tex >= 7.0.1 ghostscript texlive-ec texlive-cm-super glibc-all-langpacks
BuildRequires:	libsigsegv-devel mpfr-devel readline-devel
Requires:	filesystem >= 3

Provides:	/bin/awk
Provides:	/bin/gawk
Provides:	gawk(abi) = %{gawk_api_major}.%{gawk_api_minor}
Obsoletes:      %{name}-lang < 5.3.1

%description
The gawk package is the GNU implementation of awk.
The awk utility interprets a special-purpose programming language that
makes it possible to handle simple data-reformatting jobs with just a
few lines of code.

%package devel
Summary:          Header file for gawk extensions development
Requires:         %{name}%{?_isa} = %{version}-%{release}
%description devel
This subpackage provides /usr/include/gawkapi.h header file, which contains
definitions for use by extension functions calling into gawk.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -fv
%configure
%make_build
%make_build -C doc pdf
mkdir -p html/gawk html/gawkinet
makeinfo --html -I doc -o html/gawk     doc/gawk.texi
makeinfo --html -I doc -o html/gawkinet doc/gawkinet.texi

%check
%make_build check

%install
%make_install

rm -f ${RPM_BUILD_ROOT}%{_bindir}/gawk-%{version}*
rm -f ${RPM_BUILD_ROOT}%{_infodir}/dir

ln -sf gawk ${RPM_BUILD_ROOT}%{_bindir}/awk
ln -sf gawk.1 ${RPM_BUILD_ROOT}%{_mandir}/man1/awk.1
ln -sf /usr/share/awk   ${RPM_BUILD_ROOT}%{_datadir}/gawk
ln -sf /usr/libexec/awk ${RPM_BUILD_ROOT}%{_libexecdir}/gawk

install -m 0755 -d ${RPM_BUILD_ROOT}%{_docdir}/%{name}/html/gawk/
install -m 0755 -d ${RPM_BUILD_ROOT}%{_docdir}/%{name}/html/gawkinet/
install -m 0644 -p html/gawk/*           ${RPM_BUILD_ROOT}%{_docdir}/%{name}/html/gawk/
install -m 0644 -p html/gawkinet/*       ${RPM_BUILD_ROOT}%{_docdir}/%{name}/html/gawkinet/
install -m 0644 -p doc/gawk.{pdf,ps}     ${RPM_BUILD_ROOT}%{_docdir}/%{name}
install -m 0644 -p doc/gawkinet.{pdf,ps} ${RPM_BUILD_ROOT}%{_docdir}/%{name}

%find_lang %{name}

%files -f %{name}.lang
%doc NEWS README POSIX.STD
%license COPYING
%{_bindir}/*awk
%{_bindir}/gawkbug
%{_libdir}/*awk
%{_datadir}/*awk
%{_libexecdir}/*awk
%{_sysconfdir}/profile.d/gawk.*

%files devel
%{_includedir}/gawkapi.h

%files help
%doc NEWS POSIX.STD README_d/README.multibyte
%doc %{_docdir}/%{name}/gawk.{pdf,ps}
%doc %{_docdir}/%{name}/gawkinet.{pdf,ps}
%doc %{_docdir}/%{name}/html
%{_mandir}/man?/*
%{_infodir}/*

%changelog
* Thu Sep 19 2024 Funda Wang <fundawang@yeah.net> - 5.3.1-1
- update to 5.3.1
- merge lang subpackage into main package

* Wed Jul  3 2024 warlcok <hunan@kylinos.cn> - 5.3.0-1
- update gawk to 5.3.0

* Mon Oct 9 2023 huyubiao <huyubiao@huawei.com> - 5.2.2-1
- update gawk to 5.2.2

* Thu Jan 19 2023 laokz <zhangkai@iscas.ac.cn> - 5.2.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: backport 5.2.1 patch to fix -NaN test on riscv

* Thu Jan 12 2023 Jiayi Chen <chenjiayi22@huawei.com> - 5.2.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport five patches from upstream to fix double free bug in 5.2.0-1
       backport-Fix-a-bug-with-Node_elem_new.patch
       backport-Additional-fix-for-Node_elem_new.patch
       backport-Yet-another-fix-and-test-for-Node_elem_new.patch
       backport-Fix-a-memory-leak.patch
       backport-Code-simplification-in-interpret.h.patch

* Wed Oct 19 2022 dillon chen <dillon.chen@gmail.com> - 5.2.0-1
- update to 5.2.0
- Patch5: Disable pma tests when running in linux-user emulation

* Fri Sep 2 2022 zoulin <zoulin13@h-partners.com> - 5.1.1-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:fix source code cannot be found

* Sat Apr 2 2022 zoulin <zoulin13@h-partners.com> - 5.1.1-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:fix test case probabilistic fail

* Tue Feb 8 2022 yixiangzhike <yixiangzhike007@163.com> - 5.1.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Update gawk to  5.1.1

* Mon Aug 02 2021 chenyanpanHW <chenyanpan@huawei.com> - 5.1.0-2
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Thu Jul 30 2020 yang_zhuang_zhuang <yangzhuangzhuang1@huawei.com> - 5.1.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 5.1.0

* Tue Mar 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.0.1-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Fix backward compatibility of inplace extension

* Sun Jan 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.0.1-1
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:update to 5.0.1

* Mon Dec 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.2.1-5
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:quality enhancement synchronization github patch

* Thu Aug 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.2.1-4
- Package Init
